stages:
  - static
  - build-3.8
  - test-3.8
  - build-3.12
  - test-3.12
  - doc

pycodestyle: &static
  stage: static
  image: &image-x86_64-3-12 verdigrix/splat:python3.12-dev-x86_64
  script:
    - pip3 install .[dev]
    - pycodestyle *.py splat/*.py splat/tools/*.py

pylint:
  <<: *static
  script:
    - pip3 install .[dev]
    - >
      pylint --extension-pkg-whitelist='_splat'
      setup.py
      splat.__init__
      splat.data
      splat.filters
      splat.gen
      splat.interpol
      splat.plot
      splat.scales
      splat.seq
      splat.sources

# -----------------------------------------------------------------------------
# Python 3.8

build-x86_64-3.8: &build-x86_64
  stage: build-3.8
  needs: [pylint]
  image: &image-x86_64-3-8 verdigrix/splat:python3.8-dev-x86_64
  artifacts: &build-artifacts
    paths:
      - build/lib*
      - _splat.so
  script: &build-script
    - python3 --version
    - pip3 install .
    - ln -s $(find build -name "_splat.cpython*.so") _splat.so

build-arm64-3.8: &build-arm64
  stage: build-3.8
  tags: [arm64]
  only: {refs: [main]}
  needs: [pylint]
  image: &image-arm64-3-8 verdigrix/splat:python3.8-dev-arm64
  artifacts: *build-artifacts
  script: *build-script

build-fast-x86_64-3.8: &build-fast-x86_64
  stage: build-3.8
  needs: [pylint]
  image: *image-x86_64-3-8
  artifacts: &build-fast-artifacts
    paths:
      - build-fast/lib*
      - _splat.so.fast
  script:
    - >
        CFLAGS="$CFLAGS -DSPLAT_FAST -msse2 -O3"
        python3 setup.py build --build-base=build-fast
    - >
        ln -s $(find build-fast -name "_splat.cpython*.so")
        _splat.so.fast

build-fast-arm64-3.8: &build-fast-arm64
  stage: build-3.8
  needs: [pylint]
  tags: [arm64]
  only: {refs: [main]}
  image: *image-arm64-3-8
  artifacts: *build-fast-artifacts
  script:
    - >
        CFLAGS="$CFLAGS -DSPLAT_FAST -mtune=cortex-a57 -O3"
        python3 setup.py build --build-base=build-fast
    - >
        ln -s $(find build-fast -name "_splat.cpython*.so")
        _splat.so.fast

test-x86_64-3.8: &test-x86_64
  stage: test-3.8
  image: *image-x86_64-3-8
  needs: [build-x86_64-3.8]
  script: &test-script
    - SPLAT_UNITTEST_XML=1 python3 test.py
  artifacts: &test-artifacts
    when: always
    paths: [unittest.xml]
    reports:
      junit: unittest.xml

test-arm64-3.8: &test-arm64
  stage: test-3.8
  tags: [arm64]
  only: {refs: [main]}
  image: *image-arm64-3-8
  needs: [build-arm64-3.8]
  script: *test-script
  artifacts: *test-artifacts

test-fast-x86_64-3.8: &test-fast-x86_64
  stage: test-3.8
  image: *image-x86_64-3-8
  needs: [build-x86_64-3.8, build-fast-x86_64-3.8]
  script: &test-fast-script
    - python3 fast_test.py
    - mv _splat.so _splat.so.std; ln -s _splat.so.fast _splat.so
    - python3 fast_test.py --compare

test-fast-arm64-3.8: &test-fast-arm64
  stage: test-3.8
  tags: [arm64]
  only: {refs: [main]}
  image: *image-arm64-3-8
  needs: [build-arm64-3.8, build-fast-arm64-3.8]
  script: *test-fast-script

# -----------------------------------------------------------------------------
# Python 3.12

build-x86_64-3.12:
  <<: *build-x86_64
  stage: build-3.12
  image: *image-x86_64-3-12

build-arm64-3.12:
  <<: *build-arm64
  stage: build-3.12
  image: &image-arm64-3-12 verdigrix/splat:python3.12-dev-arm64

build-fast-x86_64-3.12:
  <<: *build-fast-x86_64
  stage: build-3.12
  image: *image-x86_64-3-12

build-fast-arm64-3.12:
  <<: *build-fast-arm64
  stage: build-3.12
  image: *image-arm64-3-12

test-x86_64-3.12:
  <<: *test-x86_64
  stage: test-3.12
  image: *image-x86_64-3-12
  needs: [build-x86_64-3.12]

test-arm64-3.12:
  <<: *test-arm64
  stage: test-3.12
  image: *image-arm64-3-12
  needs: [build-arm64-3.12]

test-fast-x86_64-3.12:
  <<: *test-fast-x86_64
  stage: test-3.12
  image: *image-x86_64-3-12
  needs: [build-x86_64-3.12, build-fast-x86_64-3.12]

test-fast-arm64-3.12:
  <<: *test-fast-arm64
  stage: test-3.12
  image: *image-arm64-3-12
  needs: [build-arm64-3.12, build-fast-arm64-3.12]

# -----------------------------------------------------------------------------
# PDF documentation

manual:
  stage: doc
  image: *image-x86_64-3-12
  needs: [build-x86_64-3.12]
  artifacts:
    paths:
      - doc/_build/latex/Splat.pdf
  script:
    - pip3 install .[doc]
    - make -C doc latexpdf
