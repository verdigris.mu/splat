# SPDX-License-Identifier: LGPL-3.0-or-later
#
# Splat - splat/interpol.py
#
# Copyright (C) 2014-2015 Guillaume Tucker

"""Splat plotting tools"""

# pylint: disable=wildcard-import disable=unused-wildcard-import
from tkinter import *


# pylint: disable=too-many-locals
def spline(spline_signal, n=100, w=400, h=400):
    """Plot a spline with a line and dots"""
    if (n < 2) or (w < 2) or (h < 2):
        raise ValueError("Invalid view parameters")

    step = (spline_signal.end - spline_signal.start) / float(n - 1)
    pts = []
    ymin = ymax = None
    for i in range(n):
        x = spline_signal.start + (i * step)
        y = spline_signal.value(x)
        if ymin is None or y < ymin:
            ymin = y
        elif ymax is None or y > ymax:
            ymax = y
        pts.append(spline_signal.value(x))

    master = Tk()
    c = Canvas(master, width=w, height=h)
    c.pack()
    c.create_rectangle(0, 0, w, h, fill="white")
    xratio = float(w) / float(n - 1)
    yratio = float(h) / (ymax - ymin)
    y0 = (pts[0] - ymin) * yratio
    for x, y in enumerate(pts[1:]):
        y = (y - ymin) * yratio
        x1 = (x + 1) * xratio
        x = x * xratio
        c.create_line(x, (h - y0), x1, (h - y))
        y0 = y
    dim = w / 100
    xratio = w / (spline_signal.end - spline_signal.start)
    for x0, y0 in spline_signal.points():
        x = (x0 - spline_signal.start) * xratio
        y = h - ((y0 - ymin) * yratio)
        c.create_oval((x - dim), (y - dim), (x + dim), (y + dim))
    mainloop()
