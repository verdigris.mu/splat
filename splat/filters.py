# SPDX-License-Identifier: LGPL-3.0-or-later
#
# Splat - splat/filters.py
#
# Copyright (C) 2012-2014, 2018 Guillaume Tucker

"""Splat signal filters"""

import collections.abc
from _splat import (  # pylint: disable=unused-import
    dec_envelope,
    reverse,
    reverb,
)


class FilterChain(collections.abc.Sequence):
    """Chain of filters to process existing data

    This class is used by the :py:class:`splat.gen.Generator` classes to define
    a chain of filter functions that are run on each newly created
    :py:class:`splat.data.Fragment` instance from a sound source
    (:ref:`sources`).
    """

    def __init__(self, filters=None):
        """This class implements the :py:class:`collections.Sequence` interface
        to hold a chain of filter functions.  The initial filter functions are
        provided via the ``filters`` list.  The idea is to be able to go
        through them in a specific order over a same audio fragment so the
        output of one filter is the input of the next.
        """
        super().__init__()
        self._filters = []
        if filters is not None:
            for f in filters:
                if isinstance(f, tuple):
                    self.append(*f)
                else:
                    self.append(f)

    def __getitem__(self, i):
        return self._filters[i]

    def __len__(self):
        return len(self._filters)

    def append(self, filter_func, args=()):
        """Add a filter function to the chain.

        The filter function ``filter_func`` is added to the end of the chain,
        and the provided ``args`` tuple is associated with it to provide
        specific parameters when invoking it by
        :py:meth:`splat.filters.FilterChain.run`.
        """
        if not isinstance(args, tuple):
            raise TypeError("Invalid filter arguments, must be a tuple")
        self._filters.append((filter_func, args))

    def run(self, frag):
        """Run all the filter functions on a sound fragment.

        All the filter functions in the chain are run with their associated
        arguments on the :py:class:`splat.data.Fragment` argument ``frag``.
        """
        for f, args in self:
            f(frag, *args)


def linear_fade(frag, duration=0.01):
    """Apply a linear fade-in and fade-out on the fragment.

    For the given ``duration`` in seconds, apply a gain that varies linearly
    from 0.0 to 1.0 and then from 1.0 to 0.0 respectively at the beginning and
    end of the fragment.  This duration is adjusted evenly if the fragment is
    too short.
    """
    fade = min((frag.rate * duration), (len(frag) / 2))
    for i in range(int(fade)):
        lvl = i / fade
        for j in (i, -i):
            frag[j] = tuple((s * lvl) for s in frag[j])


def sma(frag, length):
    """Apply a simple moving average filter on the fragment

    APply a simple moving average (SMA) filter on the fragment using the number
    of samples specified by ``length``.  The filter is initialised with the
    first sample of the fragment so there's no constraint on the fragment
    length.
    """
    if len(frag) < 1:
        return
    window = [
        [frag[0][chan] for _ in range(length - 1)]
        for chan in range(frag.channels)
    ]
    filtered = [0.0 for _ in range(frag.channels)]
    for index, sample in enumerate(frag):
        for chan, chan_window in enumerate(window):
            chan_window.append(sample[chan])
            filtered[chan] = sum(chan_window) / length
            window[chan] = chan_window[1:]
        frag[index] = tuple(filtered)


def reverb_delays(level=-6.0, length=2.0, dec=0.25, rate=400):
    """Convenience function to produce delays used with the reverb filter

    This function can be used to produce a series of delay parameters to be
    used with the reverb filter.  The `level` argument is the initial amplitude
    in dB, the `length` is the duration in seconds, `dec` is the decreasing
    ratio and `rate` is the rate of the reverb delays.  It relies on some
    heuristics with other hard-coded values as a reference, each actual
    use-case should be computing its own reverb delay parameters.
    """
    n2 = int(length * rate)
    n1 = int(n2 / 80)
    n3 = int(n1 / 2)
    r2 = rate
    r1 = r2 / 2
    d = [((t / r1), level - (t * 2 * dec)) for t in range(n3)]
    d += [((t / r2), level - (t * dec)) for t in range(n1, n2)]
    return d
